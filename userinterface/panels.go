package userinterface

import "gitlab.com/egosummiki/gophyler/config"
import "gitlab.com/egosummiki/gophyler/filemanager"
import "time"

type Panel interface {
	Print(string)
	PrintBold(string)
	Reverse()
}

func DrawBottomPanel(mode UIMode, fm *filemanager.FileManager, cf *config.Config, panel Panel) {

	current_item := fm.CurrentItem()

	panel.PrintBold(mode.Name())
	panel.Print(fm.CurrentDirectory())
	panel.Print(current_item.MIME)
	panel.Reverse()
	panel.Print(current_item.ModTime.Format(time.RFC822))
	panel.Print(current_item.Perms)
}
