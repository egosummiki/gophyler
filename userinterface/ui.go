package userinterface

import "context"
import gc "github.com/rthornton128/goncurses"
import "gitlab.com/egosummiki/gophyler/config"
import "gitlab.com/egosummiki/gophyler/filemanager"
import "os"
import "sync"
import "unicode/utf8"

const debug_log = true

type UserInterface struct {
	wnd             *gc.Window
	preview         bool
	previewWidth    int
	w               int
	h               int
	modes           []UIMode
	bindings        config.BindingSet
	keyset          []int
	activeMode      int
	previewChanel   chan PreviewInfo
	topPreviewIndex int
	scroll          int
	mutex           *sync.Mutex
	wndPreview      *gc.Window
	wndDir          *gc.Window
	wndPanel        *gc.Window
	wndCmd          *gc.Window
	wndSep          *gc.Window
}

func onCurrentItemChange(ctx context.Context, old, new *filemanager.FileInfo) {
	ui := ctx.Value("ui").(*UserInterface)
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	cf := ctx.Value("config").(*config.Config)

	ui.mutex.Lock()
	defer ui.mutex.Unlock()

	_, w := ui.wndDir.MaxYX()

	if old == nil || new == nil || ui.CheckScroll(new.Id) {
		ui.renderFileLabels(ctx)
	} else {
		ctxWidth := context.WithValue(ctx, "width", w)
		printFileInfo(
			ctxWidth,
			old.Id-ui.scroll,
			*old,
			fm.IsItemSelected(*old),
			false)
		printFileInfo(
			ctxWidth,
			new.Id-ui.scroll,
			*new,
			fm.IsItemSelected(*new),
			true)
		ui.wndDir.NoutRefresh()
	}

	DrawBottomPanel(ui.CurrentMode(), fm, cf, NewPowerline(ui.wndPanel))
	gc.Update()

	ui.topPreviewIndex++
	go UpdatePreview(ctx, ui.topPreviewIndex)
}

func NewUserInterface(ctx context.Context) *UserInterface {
	cf := ctx.Value("config").(*config.Config)
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	wnd := InitNCurses()
	h, w := wnd.MaxYX()

	fm.OnCurrentChange(onCurrentItemChange)

	return &UserInterface{
		wnd:             wnd,
		preview:         cf.Preview,
		previewWidth:    0,
		w:               w,
		h:               h,
		scroll:          0,
		bindings:        cf.GetBindings(),
		topPreviewIndex: 0,
		keyset:          make([]int, 0, 8),
		previewChanel:   make(chan PreviewInfo),
		mutex:           &sync.Mutex{},
	}
}

func (ui *UserInterface) CurrentMode() UIMode {
	return ui.modes[ui.activeMode]
}

func (ui *UserInterface) ApplyMode(ctx context.Context, id int) {
	ui.activeMode = id
	ui.CurrentMode().OnApply(ctx)
}

func printFileInfo(ctx context.Context, y int, file filemanager.FileInfo, selected, current bool) {
	ui := ctx.Value("ui").(*UserInterface)
	cf := ctx.Value("config").(*config.Config)
	wid := ctx.Value("width").(int)

	var color int16
	fileTypeInfo := cf.GetFileType(file.MIMEId)

	if current {
		ui.wndDir.AttrOn(gc.A_REVERSE)
		defer ui.wndDir.AttrOff(gc.A_REVERSE)
	}
	if selected {
		ui.wndDir.ColorOn(COLOR_WHITE)
		color = COLOR_WHITE
		ui.wndDir.AttrOn(gc.A_BOLD)
		defer ui.wndDir.AttrOn(gc.A_BOLD)
	} else {
		color = ApplyColor(ui.wndDir, fileTypeInfo["color"])
		defer ui.wndDir.ColorOff(color)
	}

	ui.wndDir.MoveAddChar(y, 0, ' ')
	ui.wndDir.MovePrint(y, 1, fileTypeInfo["icon"])
	ui.wndDir.MovePrint(y, 2, "  ")

	filename_len := utf8.RuneCountInString(file.Name)

	if 4+filename_len > wid {
		ui.wndDir.MovePrint(y, 4, file.Name[:wid-4-1])
		ui.wndDir.MovePrint(y, wid-1, "…")
	} else {
		ui.wndDir.MovePrint(y, 4, file.Name)
		for i := 4 + filename_len; i < wid; i++ {
			ui.wndDir.MoveAddChar(y, i, ' ')
		}
	}
}

func (ui *UserInterface) renderFileLabels(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	hei, wid := ui.wndDir.MaxYX()
	ui.wndDir.Erase()
	fm.ForEachFile(context.WithValue(ctx, "width", wid), ui.scroll, hei, printFileInfo)
	ui.wndDir.NoutRefresh()
}

func renderSeparator(wnd *gc.Window) {
	h, _ := wnd.MaxYX()
	for i := 0; i < h; i++ {
		wnd.MovePrint(i, 0, "|")
	}
	wnd.NoutRefresh()
}

func (ui *UserInterface) CalcPreviewWidth(cf *config.Config) {
	if ui.preview {
		ui.previewWidth = int(cf.PreviewWidth * float32(ui.w))
		return
	}
	ui.previewWidth = 0
}

func (ui *UserInterface) RenderInterface(ctx context.Context) {
	cf := ctx.Value("config").(*config.Config)
	fm := ctx.Value("fileman").(*filemanager.FileManager)

	ui.mutex.Lock()
	defer ui.mutex.Unlock()

	ui.wndDir.Erase()
	DrawBottomPanel(ui.CurrentMode(), fm, cf, NewPowerline(ui.wndPanel))
	ui.renderFileLabels(ctx)
	renderSeparator(ui.wndSep)
	gc.Update()
	ui.topPreviewIndex++
	go UpdatePreview(ctx, ui.topPreviewIndex)
}

func (ui *UserInterface) OnKeyEvent(ctx context.Context, key gc.Key) {
	if key >= 300 {
		return
	}
	if key == 27 || len(ui.keyset) == 8 {
		ui.keyset = make([]int, 0, 8)
		ui.ApplyMode(ctx, UI_MODE_NORMAL)
		return
	}
	ui.keyset = append(ui.keyset, int(key))
	actionID := ui.bindings.Get(ui.keyset)
	if actionID != 0 {
		actions[actionID](ctx)
		ui.keyset = make([]int, 0, 8)
	}
}

func (ui *UserInterface) ResizeWindows() {
	ui.wndDir.Resize(ui.h-4, ui.w-ui.previewWidth-4)
	ui.wndPanel.Resize(1, ui.w)
	ui.wndPanel.MoveWindow(ui.h-2, 0)
	ui.wndPreview.Resize(ui.h-4, ui.previewWidth)
	ui.wndPreview.MoveWindow(1, ui.w-ui.previewWidth-1)
	ui.wndCmd.Resize(1, ui.w)
	ui.wndCmd.MoveWindow(ui.h-1, 0)
	ui.wndSep.Resize(ui.h-4, 2)
	ui.wndSep.MoveWindow(1, ui.w-ui.previewWidth-3)
}

func (ui *UserInterface) CreateWindows() {
	var err error
	h, w := ui.wnd.MaxYX()

	ui.wndPreview, err = gc.NewWindow(h-4, ui.previewWidth, 1, w-ui.previewWidth-1)
	RaiseFatalError(ui.wnd, err)

	ui.wndPanel, err = gc.NewWindow(1, w, h-2, 0)
	RaiseFatalError(ui.wnd, err)

	ui.wndDir, err = gc.NewWindow(h-4, w-ui.previewWidth-4, 1, 1)
	ui.wndDir.Keypad(true)
	RaiseFatalError(ui.wnd, err)

	ui.wndCmd, err = gc.NewWindow(1, w, h-1, 0)
	RaiseFatalError(ui.wnd, err)

	ui.wndSep, err = gc.NewWindow(h-4, 2, 1, w-ui.previewWidth-3)
	RaiseFatalError(ui.wnd, err)
	renderSeparator(ui.wndSep)
}

func (ui *UserInterface) OnResize(ctx context.Context, resize_channel chan os.Signal) {
	cf := ctx.Value("config").(*config.Config)
	for {
		<-resize_channel

		ui.mutex.Lock()
		gc.End()
		gc.Update()
		ui.wnd.Erase()
		ui.wnd.NoutRefresh()
		ui.h, ui.w = ui.wnd.MaxYX()
		ui.CalcPreviewWidth(cf)
		ui.ResizeWindows()
		ui.mutex.Unlock()

		ui.RenderInterface(ctx)
	}
}

func (ui *UserInterface) CreateUIModes() {
	ui.modes = make([]UIMode, UI_MODE_NUM)
	ui.modes[UI_MODE_NORMAL] = &UIModeNormal{}
	ui.modes[UI_MODE_VISUAL] = &UIModeNormal{}
	ui.modes[UI_MODE_QUICK_RUN] = &UIModeQuickRun{}
	ui.modes[UI_MODE_FILTER] = &UIModeFilter{}
	ui.modes[UI_MODE_RENAME] = &UIModeNormal{}
	ui.modes[UI_MODE_SELECT] = &UIModeSelect{}
}

func (ui *UserInterface) CheckScroll(currentIndex int) bool {
	h, _ := ui.wndDir.MaxYX()
	relativeSelectedItem := currentIndex - ui.scroll
	if relativeSelectedItem > h-1 {
		ui.scroll = currentIndex - h + 1
		return true
	}
	if relativeSelectedItem < 0 {
		ui.scroll = currentIndex
		return true
	}
	return false
}
