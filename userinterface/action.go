package userinterface

import "context"
import gc "github.com/rthornton128/goncurses"
import "gitlab.com/egosummiki/gophyler/config"
import "gitlab.com/egosummiki/gophyler/filemanager"
import "os"

// action(fm, ui, cf)

type Action func(context.Context)

func actionMoveDown(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.MoveDown(ctx)
}

func actionMoveUp(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.MoveUp(ctx)
}

func actionOpen(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.Open(ctx)
}

func actionGoBack(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.Back(ctx)
}

func actionExecTerm(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.ExecTerm()
}

func actionHidePreview(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	cf := ctx.Value("config").(*config.Config)

	ui.preview = !ui.preview
	ui.CalcPreviewWidth(cf)
	ui.wnd.Erase()
	ui.RenderInterface(ctx)
}

func actionWidenPreview(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	cf := ctx.Value("config").(*config.Config)

	if ui.preview {
		ui.previewWidth += 5
		cf.PreviewWidth = float32(ui.previewWidth) / float32(ui.w)
		ui.wnd.Erase()
		ui.ResizeWindows()
		ui.RenderInterface(ctx)
	}
}

func actionShortenPreview(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	cf := ctx.Value("config").(*config.Config)

	if ui.preview {
		ui.previewWidth -= 5
		cf.PreviewWidth = float32(ui.previewWidth) / float32(ui.w)
		ui.wnd.Erase()
		ui.ResizeWindows()
		ui.RenderInterface(ctx)
	}
}

func actionMoveTop(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.CurrentToTop(ctx)
}

func actionMoveBottom(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.CurrentToBottom(ctx)
}

func actionGoHome(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.Goto(ctx, config.GetHomeDirectory())
}

func actionRecenter(ctx context.Context) {
	//ui := ctx.Value("ui").(*UserInterface)
	//fm := ctx.Value("fileman").(*filemanager.FileManager)
	//fm.SelectItem(fm.selectedItem - (ui.h-2)/2)
	//ui.UpdateFileLabel(ctx)
}

func actionQuit(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	ui.wnd.Delete()
	gc.End()
	os.Exit(0)
}

func actionQuickRun(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	ui.ApplyMode(ctx, UI_MODE_QUICK_RUN)
}

func actionFilter(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	ui.ApplyMode(ctx, UI_MODE_FILTER)
}

func actionSelect(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	ui.ApplyMode(ctx, UI_MODE_SELECT)
}

func actionDelete(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.Remove(ctx)
}

var actions []Action = []Action{
	nil,
	actionMoveDown,
	actionMoveUp,
	actionOpen,
	actionGoBack,
	actionExecTerm,
	actionHidePreview,
	actionWidenPreview,
	actionShortenPreview,
	actionMoveTop,
	actionMoveBottom,
	actionGoHome,
	actionRecenter,
	actionQuit,
	actionQuickRun,
	actionFilter,
	actionDelete,
	actionSelect,
}
