package userinterface

import (
	"context"
)

const (
	UI_MODE_NORMAL    = 0
	UI_MODE_VISUAL    = 1
	UI_MODE_QUICK_RUN = 2
	UI_MODE_FILTER    = 3
	UI_MODE_RENAME    = 4
	UI_MODE_SELECT    = 5

	UI_MODE_NUM = 6
)

type UIMode interface {
	Update(ctx context.Context)
	OnApply(ctx context.Context)
	Name() string
	ID() int
}
