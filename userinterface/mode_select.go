package userinterface

import "context"
import "gitlab.com/egosummiki/gophyler/filemanager"

type UIModeSelect struct{}

func (mode *UIModeSelect) Update(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	fm := ctx.Value("fileman").(*filemanager.FileManager)

	ch := ui.wndDir.GetChar()
	ui.OnKeyEvent(ctx, ch)
	fm.SelectCurrent(ctx)
}

func (mode *UIModeSelect) OnApply(ctx context.Context) {
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	fm.SelectCurrent(ctx)
}

func (mode *UIModeSelect) Name() string {
	return "VISUAL"
}

func (mode *UIModeSelect) ID() int {
	return UI_MODE_SELECT
}
