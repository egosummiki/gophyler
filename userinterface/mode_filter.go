package userinterface

import "context"
import gc "github.com/rthornton128/goncurses"
import "gitlab.com/egosummiki/gophyler/config"
import "gitlab.com/egosummiki/gophyler/filemanager"

type UIModeFilter struct {
	searchStr string
}

func (mode *UIModeFilter) Update(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	fm := ctx.Value("fileman").(*filemanager.FileManager)

	ch := ui.wndCmd.GetChar()
	switch ch {
	case 127: //Backspace
		if len(mode.searchStr) <= 1 {
			ui.ApplyMode(ctx, UI_MODE_NORMAL)
			fm.ReloadFolder(ctx)
			return
		}
		mode.searchStr = mode.searchStr[:len(mode.searchStr)-1]
		fm.ReloadFolder(ctx)
	case 27: //ESC
		ui.ApplyMode(ctx, UI_MODE_NORMAL)
		fm.ReloadFolder(ctx)
		return
	case 10: //Enter
		ui.ApplyMode(ctx, UI_MODE_NORMAL)
		return
	default:
		mode.searchStr += gc.KeyString(ch)
		break
	}

	fm.Filter(ctx, mode.searchStr)
	ui.wndCmd.MovePrint(0, 1, mode.searchStr+" ")
	ui.RenderInterface(ctx)
}

func (mode *UIModeFilter) OnApply(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	cf := ctx.Value("config").(*config.Config)
	fm := ctx.Value("fileman").(*filemanager.FileManager)

	DrawBottomPanel(ui.CurrentMode(), fm, cf, NewPowerline(ui.wndPanel))
	mode.searchStr = ""
	ui.wndCmd.MovePrint(0, 0, "/")
	ui.wndCmd.NoutRefresh()
	gc.Update()
}

func (mode *UIModeFilter) Name() string {
	return "QUICK RUN"
}

func (mode *UIModeFilter) ID() int {
	return UI_MODE_QUICK_RUN
}
