package userinterface

import "context"

type UIModeNormal struct{}

func (mode *UIModeNormal) Update(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)

	ch := ui.wndDir.GetChar()
	ui.OnKeyEvent(ctx, ch)
}

func (mode *UIModeNormal) OnApply(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	ui.wndCmd.Erase()
	ui.wndCmd.NoutRefresh()
}

func (mode *UIModeNormal) Name() string {
	return "NORMAL"
}

func (mode *UIModeNormal) ID() int {
	return UI_MODE_NORMAL
}
