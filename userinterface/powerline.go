package userinterface

import gc "github.com/rthornton128/goncurses"

const POWERLINE_COLOR_MIN = 10
const POWERLINE_COLOR_MAX = 14

type Powerline struct {
	y, x     int
	max_x    int
	col      int16
	reversed bool
	window   *gc.Window
}

func NewPowerline(window *gc.Window) *Powerline {
	window.Erase()
	window.NoutRefresh()
	return &Powerline{0, 0, 0, POWERLINE_COLOR_MIN, false, window}
}

func (p *Powerline) perform_draw(info string, drawInfo func(*Powerline, string)) {

	if p.reversed {
		p.x -= len(info)

		p.window.ColorOn(p.col)
		drawInfo(p, info)
		p.window.ColorOff(p.col)

		p.col++
		p.x--

		if p.col <= POWERLINE_COLOR_MAX {
			p.window.ColorOn(p.col)
			p.window.MovePrint(p.y, p.x, "")
			p.window.ColorOff(p.col)
		}

		p.col++
	} else {
		p.window.ColorOn(p.col)
		drawInfo(p, info)
		p.window.ColorOff(p.col)

		p.x += len(info)
		p.col++

		if p.col <= POWERLINE_COLOR_MAX {
			p.window.ColorOn(p.col)
			p.window.MovePrint(p.y, p.x, "")
			p.window.ColorOff(p.col)
		}

		p.x++
		p.col++
	}

	p.window.NoutRefresh()
}

func PrintMessage(p *Powerline, info string) {
	p.window.MovePrint(p.y, p.x, info)
}

func PrintMessageBold(p *Powerline, info string) {
	p.window.AttrOn(gc.A_BOLD)
	p.window.MovePrint(p.y, p.x, info)
	p.window.AttrOff(gc.A_BOLD)
}

func (p *Powerline) Print(info string) {
	p.perform_draw(" "+info+" ", PrintMessage)
	p.window.NoutRefresh()
}

func (p *Powerline) PrintBold(info string) {
	p.perform_draw(" "+info+" ", PrintMessageBold)
	p.window.NoutRefresh()
}

func (p *Powerline) Reverse() {

	_, w := p.window.MaxYX()

	p.reversed = true
	p.col = POWERLINE_COLOR_MIN
	p.max_x = p.x
	p.x = w
	p.window.NoutRefresh()
}
