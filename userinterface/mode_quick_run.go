package userinterface

import "context"
import gc "github.com/rthornton128/goncurses"
import "gitlab.com/egosummiki/gophyler/config"
import "gitlab.com/egosummiki/gophyler/filemanager"

type UIModeQuickRun struct {
	search_str string
}

func (mode *UIModeQuickRun) Update(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	fm := ctx.Value("fileman").(*filemanager.FileManager)

	ch := ui.wndCmd.GetChar()
	switch ch {
	case 127:
		if len(mode.search_str) <= 1 {
			ui.ApplyMode(ctx, UI_MODE_NORMAL)
			fm.ReloadFolder(ctx)
			return
		}
		mode.search_str = mode.search_str[:len(mode.search_str)-1]
		fm.ReloadFolder(ctx)
	case 27:
		ui.ApplyMode(ctx, UI_MODE_NORMAL)
		fm.ReloadFolder(ctx)
		return
	case 10:
		ui.ApplyMode(ctx, UI_MODE_NORMAL)
		fm.Open(ctx)
		return
	default:
		mode.search_str += gc.KeyString(ch)
	}
	fm.Filter(ctx, mode.search_str)
	ui.wndCmd.MovePrint(0, 2, mode.search_str)
	ui.RenderInterface(ctx)

	if fm.NumFiles() == 1 {
		ui.ApplyMode(ctx, UI_MODE_NORMAL)
		fm.Open(ctx)
		return
	}
}

func (mode *UIModeQuickRun) OnApply(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	cf := ctx.Value("config").(*config.Config)
	fm := ctx.Value("fileman").(*filemanager.FileManager)

	DrawBottomPanel(ui.CurrentMode(), fm, cf, NewPowerline(ui.wndPanel))
	mode.search_str = ""
	ui.wndCmd.MovePrint(0, 0, "f ")
	ui.wndCmd.NoutRefresh()
	gc.Update()
}

func (mode *UIModeQuickRun) Name() string {
	return "QUICK RUN"
}

func (mode *UIModeQuickRun) ID() int {
	return UI_MODE_QUICK_RUN
}
