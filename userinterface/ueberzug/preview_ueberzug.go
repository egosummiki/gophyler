package ueberzug

// #cgo CFLAGS: -I/usr/include/python3.7m
// #cgo LDFLAGS: -lpython3.7m
// #include "ueberzug.h"
import "C"

type PythonEngine C.PythonEngine

func NewPythonEngine() *PythonEngine {
	return (*PythonEngine)(C.init_python())
}

func (py *PythonEngine) DrawImage(path string, y, x, h, w int) {
	_ = path
	_ = x
	_ = y
	_ = h
	_ = w
	// C.draw_image((*C.PythonEngine)(py), C.CString(path), C.int(y), C.int(x), C.int(h), C.int(w))
}

func (py *PythonEngine) HideImage() {
	// C.hide_image((*C.PythonEngine)(py))
}

func (py *PythonEngine) Release() {
	_ = py
	C.release_python()
}
