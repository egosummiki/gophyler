import ueberzug.lib.v0 as ueberzug
import time

can = ueberzug.Canvas()
plc = None
dis = True 

def setup_placement(px, py, w, h):
    if plc != None:
        pass
    plc = can.create_placement('image-preview', x=px, y=py, max_width=w, max_height=h)

def draw_image(path, px, py, w, h):
    global can, plc, dis
    if plc == None:
        plc = can.create_placement('image-preview', x=px, y=py, max_width=w, max_height=h)
    plc.path = path
    plc.visibility = ueberzug.Visibility.VISIBLE
    while dis:
        pass

def update_image(path):
    global plc, dis
    if plc != None:
        plc.path = path
        plc.visibility = ueberzug.Visibility.VISIBLE
        dis = True
        print("Updating image")
        while dis:
            pass

def hide_image():
    global plc, dis
    if plc != None:
        if plc.visibility == ueberzug.Visibility.VISIBLE:
            plc.visibility = ueberzug.Visibility.INVISIBLE
        dis = False
