#include <Python.h>
#include <stdio.h>
#include <stdlib.h>

/*
Python script should be kept in /usr/share/gophyler/ueberzug.py
*/
#ifdef __linux__
#define PYTHON_SCRIPT "/usr/share/gophyler/ueberzug.py"
#else
#define PYTHON_SCRIPT "ueberzug.py"
#endif

typedef struct {
    PyObject *module, 
             *fn_draw_image,
             *fn_setup_placement,
             *fn_update_image,
             *fn_hide_image;
} PythonEngine;

/*
Initialize python script
*/
static PythonEngine*
init_python() {

    FILE *f;
    PyObject *module, *dict, *n_draw, *n_setup, *n_update, *n_hide;
    PythonEngine *engine = (PythonEngine*)malloc(sizeof(PythonEngine));

    Py_Initialize();
    engine->module = PyImport_AddModule("__main__");
    dict           = PyModule_GetDict(engine->module);

    f = fopen(PYTHON_SCRIPT, "r");
    if(!f) {
        return NULL;
    }
    PyRun_SimpleFile(f, PYTHON_SCRIPT);
    fclose(f);

    n_draw   = PyUnicode_FromString("draw_image");
    n_setup  = PyUnicode_FromString("setup_placement");
    n_update = PyUnicode_FromString("update_image");
    n_hide   = PyUnicode_FromString("hide_image");
    engine->fn_draw_image      = PyDict_GetItem(dict, n_draw);
    engine->fn_setup_placement = PyDict_GetItem(dict, n_setup);
    engine->fn_update_image    = PyDict_GetItem(dict, n_update);
    engine->fn_hide_image      = PyDict_GetItem(dict, n_hide);

    Py_DECREF(dict);
    Py_DECREF(n_draw);
    Py_DECREF(n_setup);

    return engine;
}

/*
Release python engine
*/
static void
release_python() {
    Py_Finalize();
}

static void
hide_image(PythonEngine *engine) {

    PyObject_CallObject(engine->fn_hide_image, NULL);
}

static void
draw_image(PythonEngine *engine, char *path, int y, int x, int h, int w) {

    PyObject *args, *o_path, *o_y, *o_x, *o_h, *o_w;

    args = PyTuple_New(5);
    o_path = PyUnicode_FromString(path);
    o_x = PyLong_FromLong(x);
    o_y = PyLong_FromLong(y);
    o_w = PyLong_FromLong(w);
    o_h = PyLong_FromLong(h);

    PyTuple_SetItem(args, 0, o_path);
    PyTuple_SetItem(args, 1, o_x);
    PyTuple_SetItem(args, 2, o_y);
    PyTuple_SetItem(args, 3, o_w);
    PyTuple_SetItem(args, 4, o_h);

    PyObject_CallObject(engine->fn_draw_image, args);

    Py_DECREF(args);
}

