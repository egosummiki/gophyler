package userinterface

import (
	"context"
	gc "github.com/rthornton128/goncurses"
	"io"
	"os"
	"os/exec"

	"gitlab.com/egosummiki/gophyler/config"
	"gitlab.com/egosummiki/gophyler/filemanager"
	"gitlab.com/egosummiki/gophyler/userinterface/ueberzug"
)

type PreviewInfo struct {
	index  int
	buffer []byte
	path   string
	kind   int
}

func GetPreviewRaw(buffer_size int, file_path string) []byte {

	var amount int
	buffer := make([]byte, buffer_size)

	file, err := os.Open(file_path)
	if err == nil {
		defer file.Close()
		amount, err = io.ReadAtLeast(file, buffer, buffer_size)
	} else {
		amount = 0
	}

	for index := range buffer[amount:] {
		buffer[index+amount] = ' '
	}

	return buffer
}

func GetPreviewStdout(fm *filemanager.FileManager, buffer_size int, command string, stderr bool) []byte {

	_ = command

	var amount int
	buffer := make([]byte, buffer_size)

	var cmd *exec.Cmd

	cmd = fm.ParseCommand(command)

	var err error
	var stdout io.ReadCloser

	if stderr {
		stdout, err = cmd.StderrPipe()
	} else {
		stdout, err = cmd.StdoutPipe()
	}

	if err == nil {
		cmd.Start()
		amount, err = io.ReadAtLeast(stdout, buffer, buffer_size)
		stdout.Close()
		cmd.Wait()
	} else {
		amount = 0
	}

	for index := range buffer[amount:] {
		buffer[index+amount] = ' '
	}

	return buffer
}

/*
Kind:
	0 - Fail
	1 - Buffer display
	2 - Image
*/
func UpdatePreview(ctx context.Context, index int) {
	ui := ctx.Value("ui").(*UserInterface)
	cf := ctx.Value("config").(*config.Config)
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	py := ctx.Value("python").(*ueberzug.PythonEngine)

	var preview string
	var ok bool
	var info PreviewInfo

	if !ui.preview {
		return
	}

	py.HideImage()
	info.index = index

	filetype := cf.GetFileType(fm.CurrentItem().MIMEId)

	if preview, ok = filetype["preview_stdout"]; ok {
		info.buffer = GetPreviewStdout(fm, ui.previewWidth*(ui.h-3), preview, false)
		info.kind = 1
		ui.previewChanel <- info
		return
	} else if preview, ok = filetype["preview_stderr"]; ok {
		info.buffer = GetPreviewStdout(fm, ui.previewWidth*(ui.h-3), preview, true)
		info.kind = 1
		ui.previewChanel <- info
		return
	} else if preview, ok = filetype["preview"]; ok {

		info.path = fm.CurrentDirectory() + "/" + fm.CurrentItem().Name

		if preview == "ubz" {

			info.kind = 2
			ui.previewChanel <- info
			return
		}

		info.buffer = GetPreviewRaw(ui.previewWidth*(ui.h-3), info.path)
		info.kind = 1
		ui.previewChanel <- info
		return
	}
}

func PreviewRoutine(ctx context.Context) {
	ui := ctx.Value("ui").(*UserInterface)
	py := ctx.Value("python").(*ueberzug.PythonEngine)

	for {
		info := <-ui.previewChanel

		if ui.topPreviewIndex != info.index {
			continue
		}

		switch info.kind {
		case 1:
			ui.mutex.Lock()
			ui.wndPreview.Erase()
			ui.wndPreview.MovePrint(0, 0, string(info.buffer))
			ui.wndPreview.NoutRefresh()
			gc.Update()
			ui.mutex.Unlock()
		case 2:
			y, x := ui.wndPreview.YX()

			ui.mutex.Lock()
			ui.wndPreview.Erase()
			ui.wndPreview.NoutRefresh()
			gc.Update()
			ui.mutex.Unlock()

			py.DrawImage(info.path, y, x, ui.h-3, ui.previewWidth)
		}
	}

}
