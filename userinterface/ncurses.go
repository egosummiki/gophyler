package userinterface

import gc "github.com/rthornton128/goncurses"
import "log"
import "os"

const COLOR_ERROR = 2
const COLOR_YELLOW = 3
const COLOR_RED = 4
const COLOR_BLUE = 5
const COLOR_GREEN = 6
const COLOR_PREVIEW = 7
const COLOR_WHITE = 8

const COLOR_PL_BLUE = 10
const COLOR_PL_BLUE_PASS = 11
const COLOR_PL_WHITE = 12
const COLOR_PL_WHITE_PASS = 13
const COLOR_PL_NORM = 14

const KEY_ARROW_DOWN = 258
const KEY_ARROW_UP = 259
const KEY_ARROW_LEFT = 260
const KEY_ARROW_RIGHT = 261
const KEY_ARROW_ENTER = 10
const KEY_ST_SCROLL_UP = 25
const KEY_ST_SCROLL_DOWN = 5
const KEY_CTL_N = 14

func InitNCurses() (window *gc.Window) {

	// Initialise n-curses
	window, err := gc.Init()
	if err != nil {
		raiseError(err)
	}

	initColors()

	gc.Echo(false)
	gc.CBreak(true)
	gc.Cursor(0)

	return
}

func ApplyColor(window *gc.Window, color string) int16 {

	switch color {
	case "default":
		fallthrough
	default:
		gc.UseDefaultColors()
		return 0
	case "yellow":
		window.ColorOn(COLOR_YELLOW)
		return COLOR_YELLOW
	case "blue":
		window.ColorOn(COLOR_BLUE)
		return COLOR_BLUE
	case "red":
		window.ColorOn(COLOR_RED)
		return COLOR_RED
	case "green":
		window.ColorOn(COLOR_GREEN)
		return COLOR_GREEN
	}
}

func RaiseFatalError(window *gc.Window, err error) {

	if err == nil {
		return
	}

	h, w := window.MaxYX()

	window.Erase()
	window.ColorOn(COLOR_ERROR)
	window.MovePrint(h/2, w/2-len(err.Error())/2, err)
	window.ColorOff(COLOR_ERROR)
	window.Refresh()
	window.GetChar()

	os.Exit(2)
}

func raiseError(err error) {
	log.Fatal(err)
}

func initColors() {

	err := gc.StartColor()
	if err != nil {
		raiseError(err)
	}

	gc.UseDefaultColors()

	gc.InitPair(1, gc.C_WHITE, gc.C_BLACK)
	gc.InitPair(COLOR_ERROR, gc.C_BLACK, gc.C_RED)
	gc.InitPair(COLOR_YELLOW, gc.C_YELLOW, -1)
	gc.InitPair(COLOR_RED, gc.C_RED, -1)
	gc.InitPair(COLOR_BLUE, gc.C_BLUE, -1)
	gc.InitPair(COLOR_GREEN, gc.C_GREEN, -1)
	gc.InitPair(COLOR_PREVIEW, gc.C_WHITE+8, gc.C_BLACK)
	gc.InitPair(COLOR_WHITE, gc.C_WHITE+8, -1)

	gc.InitPair(COLOR_PL_BLUE, gc.C_BLACK, gc.C_BLUE+8)
	gc.InitPair(COLOR_PL_BLUE_PASS, gc.C_BLUE+8, gc.C_WHITE)
	gc.InitPair(COLOR_PL_WHITE, gc.C_BLACK, gc.C_WHITE)
	gc.InitPair(COLOR_PL_WHITE_PASS, gc.C_WHITE, -1)
	gc.InitPair(COLOR_PL_NORM, -1, -1)
}
