package filemanager

import (
	"io"
	"os"
	"strconv"
)

type FileTransfer struct {
	source_dir string
	dest_dir   string
	files      []string
}

func FindAppropriateName(file_name string) string {

	if _, err := os.Stat(file_name); os.IsNotExist(err) {
		return file_name
	}

	num := 0
	dec := 1
	index := len(file_name) - 1

	for ; index >= 0; index-- {

		ch := file_name[index]

		if ch < '0' || ch > '9' {
			break
		}

		num += dec * int(ch-'0')
		dec *= 10
	}

	return FindAppropriateName(file_name[:(index+1)] + strconv.Itoa(num+1))
}

func PerformCopyFile(source_name, dest_name string) error {

	source, err := os.Open(source_name)

	if err != nil {
		return err
	}
	defer source.Close()

	target, err := os.OpenFile(dest_name, os.O_RDWR|os.O_CREATE, 0666)

	if err != nil {
		return err
	}
	defer target.Close()

	_, err = io.Copy(target, source)

	if err != nil {
		return err
	}

	return nil
}

func PerformCopyNoreplace(ft *FileTransfer) {

	for _, file := range ft.files {
		PerformCopyFile(ft.source_dir+"/"+file, FindAppropriateName(ft.dest_dir+"/"+file))
	}
}

func PerformCopy(ft *FileTransfer) {

	for _, file := range ft.files {
		PerformCopyFile(ft.source_dir+"/"+file, ft.dest_dir+"/"+file)
	}
}
