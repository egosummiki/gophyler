package filemanager

import (
	"os"
	"time"

	"gitlab.com/egosummiki/gophyler/config"
)

const (
	FileTypeNormal     = 0
	FileTypeDirectory  = 1
	FileTypeLink       = 2
	FileTypeExecutable = 3
)

type FileInfo struct {
	Id       int
	Name     string
	MIME     string
	MIMEId   int
	ModTime  time.Time
	Perms    string
	FileType int
}

func NewFileInfo(id int, cf *config.Config, file os.FileInfo) FileInfo {

	fileMIME := cf.GetMimeName(file)
	fileType := FileTypeNormal

	if file.IsDir() {
		fileType = FileTypeLink
	} else if file.Mode()&os.ModeSymlink != 0 {
		fileType = FileTypeLink
	} else if file.Mode().Perm()%2 == 1 {
		fileType = FileTypeExecutable
	}

	return FileInfo{
		Id:       id,
		Name:     file.Name(),
		MIME:     fileMIME,
		MIMEId:   cf.GetMimeId(fileMIME),
		ModTime:  file.ModTime(),
		Perms:    file.Mode().String(),
		FileType: fileType}
}
