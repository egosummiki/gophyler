package filemanager

import (
	"context"
	gc "github.com/rthornton128/goncurses"
	iou "io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strings"

	"gitlab.com/egosummiki/gophyler/config"
)

type ForEachCallback func(context.Context, int, FileInfo, bool, bool)
type ChangeCallback func(context.Context, *FileInfo, *FileInfo)

type FileManager struct {
	currentItem      int
	pathHistory      []int
	currentDirectory string
	itemsTotal       int
	content          []FileInfo
	showHiddenFiles  bool
	selection        map[string]bool
	changeCallback   ChangeCallback
}

func NewFileManager() *FileManager {
	dir, err := os.Getwd()
	if err != nil {
		return nil
	}

	return &FileManager{
		currentItem:      0,
		pathHistory:      make([]int, 0, 5),
		currentDirectory: dir,
		itemsTotal:       0,
		content:          nil,
		selection:        make(map[string]bool),
		showHiddenFiles:  false,
	}
}

func (fm *FileManager) ForEachFile(ctx context.Context, start, limit int, callback ForEachCallback) {
	if start >= len(fm.content) {
		return
	}
	for i := start; i < len(fm.content) && i < start+limit; i++ {
		callback(
			ctx,
			i-start,
			fm.content[i],
			fm.selection[fm.currentDirectory+"/"+fm.content[i].Name],
			fm.currentItem == i)
	}
}

func (fm *FileManager) setCurrentItem(ctx context.Context, id int) {
	var oldItem, newItem *FileInfo

	previous := fm.currentItem
	fm.currentItem = id

	if len(fm.content) > 0 {
		if previous < 0 {
			previous = 0
		} else if previous >= len(fm.content) {
			previous = len(fm.content) - 1
		}
		oldItem = &fm.content[previous]
		if fm.currentItem < 0 {
			fm.currentItem = 0
		} else if fm.currentItem >= len(fm.content) {
			fm.currentItem = len(fm.content) - 1
		}
		newItem = &fm.content[fm.currentItem]
	}

	fm.changeCallback(ctx, oldItem, newItem)
}

func (fm *FileManager) OnCurrentChange(callback ChangeCallback) {
	fm.changeCallback = callback
}

func (fm *FileManager) IsItemSelected(item FileInfo) bool {
	return fm.selection[fm.currentDirectory+"/"+item.Name]
}

func (fm *FileManager) currentFile() string {
	return fm.currentDirectory + "/" + fm.CurrentItem().Name
}

func (fm *FileManager) CurrentDirectory() string {
	return fm.currentDirectory
}

func (fm *FileManager) NumFiles() int {
	return len(fm.content)
}

func (fm *FileManager) ReloadFolder(ctx context.Context) {
	cf := ctx.Value("config").(*config.Config)
	fm.UpdateFileInfo(cf)
	fm.changeCallback(ctx, nil, nil)
}

func (fm *FileManager) Goto(ctx context.Context, path string) {
	fm.currentDirectory = path
	fm.changeCallback(ctx, nil, nil)
}

func (fm *FileManager) TotalItems() int {
	return fm.itemsTotal
}

func (fm *FileManager) SelectCurrent(ctx context.Context) {
	fm.selection[fm.currentFile()] = true
	fm.changeCallback(ctx, nil, nil)
}

func (fm *FileManager) ClearSelection() {
	fm.selection = make(map[string]bool)
}

func (fm *FileManager) Remove(ctx context.Context) {
	if len(fm.selection) == 0 {
		fm.selection[fm.currentFile()] = true
	}
	for key, val := range fm.selection {
		if val {
			os.Remove(key)
		}
	}
	fm.ClearSelection()
	fm.changeCallback(ctx, nil, nil)
}

func (fm *FileManager) ZeroCurrentItem(ctx context.Context) {
	fm.setCurrentItem(ctx, 0)
}

func (fm *FileManager) CurrentItem() *FileInfo {
	if fm.itemsTotal == 0 {
		return &FileInfo{}
	}
	for fm.currentItem >= fm.itemsTotal {
		fm.currentItem--
	}
	return &fm.content[fm.currentItem]
}

func (fm *FileManager) CurrentToTop(ctx context.Context) {
	fm.setCurrentItem(ctx, 0)
}

func (fm *FileManager) CurrentToBottom(ctx context.Context) {
	fm.setCurrentItem(ctx, len(fm.content)-1)
}

func (fm *FileManager) UpdateFileInfo(cf *config.Config) {
	fileInfo, err := iou.ReadDir(fm.currentDirectory)
	if err != nil {
		return
	}

	if fm.showHiddenFiles {
		fm.itemsTotal = len(fileInfo)
		fm.content = make([]FileInfo, fm.itemsTotal)
		for i, file := range fileInfo {
			fm.content[i] = NewFileInfo(i, cf, file)
		}
	} else {
		fm.itemsTotal = 0
		for _, file := range fileInfo {
			if file.Name()[0] != '.' {
				fm.itemsTotal++
			}
		}

		fm.content = make([]FileInfo, fm.itemsTotal)

		i := 0
		for _, file := range fileInfo {
			if file.Name()[0] != '.' {
				fm.content[i] = NewFileInfo(i, cf, file)
				i++
			}
		}
	}

	if fm.currentItem >= len(fm.content) {
		fm.currentItem = len(fm.content) - 1
	}
}

func (fm *FileManager) MoveUp(ctx context.Context) {
	if fm.currentItem > 0 {
		fm.setCurrentItem(ctx, fm.currentItem-1)
	}
}

func (fm *FileManager) MoveDown(ctx context.Context) {
	if fm.currentItem < fm.itemsTotal-1 {
		fm.setCurrentItem(ctx, fm.currentItem+1)
	}
}

func (fm *FileManager) ExecTerm() {

	os.Chdir(fm.currentDirectory)
	cmd := exec.Command("zsh")
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	gc.End()
	cmd.Run()
	gc.Init()
}

func (fm *FileManager) ParseCommand(command string) *exec.Cmd {
	cmdArguments := strings.Split(command, " ")

	for i, arg := range cmdArguments {

		if arg == "%f" {
			cmdArguments[i] = fm.currentDirectory + "/" + fm.CurrentItem().Name
		}
	}

	return exec.Command(cmdArguments[0], cmdArguments[1:]...)
}

func (fm *FileManager) Open(ctx context.Context) {
	cf := ctx.Value("config").(*config.Config)

	fm.pathHistory = append(fm.pathHistory, fm.currentItem)

	fileInfo := fm.CurrentItem()
	if fileInfo.FileType == FileTypeDirectory || fileInfo.FileType == FileTypeLink {

		if len(fm.currentDirectory) == 1 {
			fm.currentDirectory = fm.currentDirectory + fileInfo.Name
		} else {
			fm.currentDirectory = fm.currentDirectory + "/" + fileInfo.Name
		}
		fm.UpdateFileInfo(cf)
		fm.changeCallback(ctx, nil, nil)
	} else {

		mimeInfo := cf.GetFileType(fileInfo.MIMEId)
		var cmdStr string
		var ok bool

		if cmdStr, ok = mimeInfo["exec"]; ok {

			cmd := fm.ParseCommand(cmdStr)
			cmd.Start()

		} else if cmdStr, ok = mimeInfo["exec_term"]; ok {

			cmd := fm.ParseCommand(cmdStr)
			cmd.Stdout = os.Stdout
			cmd.Stdin = os.Stdin
			cmd.Stderr = os.Stderr
			gc.End()
			cmd.Run()
			gc.Update()
		}

	}

}

func (fm *FileManager) Back(ctx context.Context) {
	cf := ctx.Value("config").(*config.Config)

	if fm.currentDirectory == "/" {
		return
	}

	slashAt := len(fm.currentDirectory) - 1
	for fm.currentDirectory[slashAt] != '/' {
		slashAt--
	}
	if slashAt == 0 {
		fm.currentDirectory = "/"
	} else {
		fm.currentDirectory = fm.currentDirectory[0:slashAt]
	}

	fm.UpdateFileInfo(cf)

	if len(fm.pathHistory) > 0 {
		fm.currentItem = fm.pathHistory[len(fm.pathHistory)-1]
		fm.pathHistory = fm.pathHistory[:len(fm.pathHistory)-1]
	}
	fm.changeCallback(ctx, nil, nil)

}

func (fm *FileManager) Filter(ctx context.Context, cond string) {
	newContent := make([]FileInfo, 0, len(fm.content))

	numMatched := 0
	for _, file := range fm.content {

		if matched, _ := regexp.MatchString(cond, file.Name); matched {
			file.Id = numMatched
			newContent = append(newContent, file)
			numMatched++
		}
	}

	fm.content = newContent
	fm.itemsTotal = len(fm.content)

	if fm.currentItem >= len(newContent) {
		fm.changeCallback(ctx, nil, nil)
	}
}
