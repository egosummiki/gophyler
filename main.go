package main

import (
	// #cgo LDFLAGS: -lncursesw
	// #include <locale.h>
	"C"
	"context"
	gc "github.com/rthornton128/goncurses"
	"log"
	"os"
	"os/signal"
	"runtime/debug"
	"syscall"

	"gitlab.com/egosummiki/gophyler/config"
	"gitlab.com/egosummiki/gophyler/filemanager"
	"gitlab.com/egosummiki/gophyler/userinterface"
	"gitlab.com/egosummiki/gophyler/userinterface/ueberzug"
)

func initContext() context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "fileman", filemanager.NewFileManager())
	ctx = context.WithValue(ctx, "config", config.NewConfig())
	ctx = context.WithValue(ctx, "python", ueberzug.NewPythonEngine())
	ctx = context.WithValue(ctx, "ui", userinterface.NewUserInterface(ctx))
	return ctx
}

func main() {

	defer func() {
		if r := recover(); r != nil {
			gc.End()
			debug.PrintStack()
			log.Fatal("\nPANIC: \n", r)
		}
	}()

	C.setlocale(C.LC_ALL, C.CString(""))

	ctx := initContext()
	ui := ctx.Value("ui").(*userinterface.UserInterface)
	fm := ctx.Value("fileman").(*filemanager.FileManager)
	cf := ctx.Value("config").(*config.Config)

	go userinterface.PreviewRoutine(ctx)
	ui.CreateUIModes()
	ui.CalcPreviewWidth(cf)
	ui.CreateWindows()
	fm.ReloadFolder(ctx)
	ui.RenderInterface(ctx)

	resize_channel := make(chan os.Signal)
	signal.Notify(resize_channel, syscall.SIGWINCH)

	go ui.OnResize(ctx, resize_channel)

	for {
		ui.CurrentMode().Update(ctx)
	}
}
