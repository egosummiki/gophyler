
run: all
	./gophyler

all:
	go build

clean: gophyler gop
	rm gophyler
	rm gop

install:
	mv gophyler gop
	install gop /usr/bin
