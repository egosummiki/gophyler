package config

type Binding struct {
	keys   []int
	action int
}

type BindingSet []Binding

var specialKeys map[string]int = map[string]int{
	"down":     258,
	"up":       259,
	"left":     260,
	"right":    261,
	"ret":      10,
	"smaller":  60,
	"greather": 62,
	"slash":    47,
	"st-up":    25,
	"st-down":  5,
	"cn":       14,
}

var actionIDs map[string]int = map[string]int{
	"none":           0,
	"move_down":      1,
	"move_up":        2,
	"open":           3,
	"back":           4,
	"exec_term":      5,
	"hide_preview":   6,
	"winden_preview": 7,
	"shorten":        8,
	"move_top":       9,
	"move_bottom":    10,
	"go_home":        11,
	"recenter":       12,
	"quit":           13,
	"quick_run":      14,
	"filter":         15,
	"delete":         16,
	"visual":         17,
}

func NewBindingSet(size int) (bindingSet BindingSet) {
	bindingSet = make(BindingSet, size)
	for i := range bindingSet {
		bindingSet[i] = Binding{nil, 0}
	}
	return
}

func compareKeys(a []int, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func calculateHash(keys []int) int {
	var sum int = 0
	for _, k := range keys {
		sum += k
	}
	return sum
}

func (bs BindingSet) Get(keys []int) int {
	candidate := calculateHash(keys) % len(bs)
	for candidate < len(bs) {
		if compareKeys(bs[candidate].keys, keys) {
			return bs[candidate].action
		} else {
			candidate = (candidate + 1) % len(bs)
		}
	}
	return 0
}

func (bs BindingSet) Set(keys []int, action int) {
	candidate := calculateHash(keys) % len(bs)
	for candidate < len(bs) {
		if bs[candidate].keys == nil {
			bs[candidate] = Binding{keys, action}
			return
		} else {
			candidate = (candidate + 1) % len(bs)
		}
	}
	panic("Too little space for a binding set")
}
