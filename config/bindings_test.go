package config

import (
	"testing"
)

func Test_compareKeys(t *testing.T) {
	type args struct {
		a []int
		b []int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Key test same",
			args: args{[]int{153, 234, 678}, []int{153, 234, 678}},
			want: true,
		},
		{
			name: "Key test different",
			args: args{[]int{153, 234, 678}, []int{153, 987, 678}},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := compareKeys(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("compareKeys() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculateHash(t *testing.T) {
	type args struct {
		keys []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Hash Calc",
			args: args{keys: []int{3, 4, 5}},
			want: 12,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateHash(tt.args.keys); got != tt.want {
				t.Errorf("calculateHash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBindingSet_Get(t *testing.T) {

	bindingSet := NewBindingSet(10)
	bindingSet.Set([]int{3, 6, 7}, 3)
	bindingSet.Set([]int{3, 6}, 4)
	bindingSet.Set([]int{3, 5}, 7)
	bindingSet.Set([]int{7, 1, 9}, 9)

	type args struct {
		keys []int
	}
	tests := []struct {
		name string
		bs   BindingSet
		args args
		want int
	}{
		{
			name: "Binding set Get",
			bs:   bindingSet,
			args: args{keys: []int{3, 6, 7}},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.bs.Get(tt.args.keys); got != tt.want {
				t.Errorf("BindingSet.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}
