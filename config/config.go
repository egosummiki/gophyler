package config

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"mime"
	"os"
	"os/user"
	"regexp"
)

type Config struct {
	DisplayIcons bool                `yaml:"display_icons"`
	Preview      bool                `yaml:"preview"`
	PreviewWidth float32             `yaml:"preview_width"`
	FileTypes    []map[string]string `yaml:"file_types"`
	Bindings     []map[string]string `yaml:"bindings"`
}

func GetHomeDirectory() string {
	usr, err := user.Current()
	if err != nil {
		panic("Cannot get user info")
	}

	return usr.HomeDir
}

func GetFileExtension(filename string) string {
	for i := len(filename) - 1; i >= 0; i-- {
		if filename[i] == '.' {
			return filename[i:]
		}
	}
	return ""
}

func NewConfig() *Config {
	var config Config

	data, err := ioutil.ReadFile(GetHomeDirectory() + "/.config/gophyler/config.yml")
	if err != nil {
		panic("Error parsing file")
	}

	yaml.Unmarshal(data, &config)

	return &config
}

func (cf *Config) GetBindings() BindingSet {
	result := NewBindingSet(300)

	for _, binding := range cf.Bindings {
		special := -1
		keys := make([]int, 0, 8)

		for i, ch := range binding["key"] {

			if len(keys) == 8 {
				break
			}

			if special >= 0 {
				if ch == '>' {
					num, ok := specialKeys[binding["key"][special:i]]
					if ok {
						keys = append(keys, num)
					}
					special = -1
				}
			} else {
				if ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' {
					keys = append(keys, int(ch))
				} else if ch == '<' {
					special = i + 1
				}
			}
		}

		result.Set(keys, actionIDs[binding["action"]])
	}

	return result
}

func (cf *Config) GetFileType(id int) map[string]string {
	return cf.FileTypes[id]
}

func (cf *Config) GetMimeType(mime string) map[string]string {
	for _, tp := range cf.FileTypes {

		if matched, _ := regexp.MatchString(tp["type"], mime); matched {
			return tp
		}
	}

	return map[string]string{}
}

func (cf *Config) GetMimeId(mime string) int {
	for id, tp := range cf.FileTypes {

		if matched, _ := regexp.MatchString(tp["type"], mime); matched {
			return id
		}
	}

	return 0
}

func (cf *Config) GetMimeName(fileInfo os.FileInfo) string {
	if fileInfo.IsDir() {
		return "dir"
	}
	if fileInfo.Mode()&os.ModeSymlink != 0 {
		return "link"
	}

	ext := GetFileExtension(fileInfo.Name())
	if ext == "" {
		if fileInfo.Mode().Perm()%2 == 1 {
			return "executable"
		}
		return "*"
	}

	mimeName := mime.TypeByExtension(ext)
	if len(mimeName) > 0 {
		return mimeName
	}

	return ext
}

func (cf *Config) ObtainMimeInfo(fileInfo os.FileInfo) map[string]string {

	return cf.GetMimeType(cf.GetMimeName(fileInfo))
}
