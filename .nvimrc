filetype plugin on
set omnifunc=syntaxcomplete#Complete

function BuildProject()
    sp | terminal make run
endfunction

map <F5> :call BuildProject()<return>
