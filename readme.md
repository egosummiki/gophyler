# Gophyler

A text-based file manager inspired by vim written in the go programming language.

## Installation

For ueberzug image preview file "ueberzug.py" needs to be copied to **/usr/share/gophyler** directory

Config file needs to be present at **_$HOME_/.config/gophyler/config.yml**

## Configuration

Example configuration file

```yaml
# Configuration for gophyler
#
#

display_icons: true

# Preview
preview: true
preview_width: 0.6 # How much the preview takes up on screen

# Default actions on given files types
# you specify a given type with a regex
# the filetype is determined by:
#   1. Is it a directory -> dir
#   2. No, is it a simlink? -> link
#   3. Does it have mime name? -> [mime name]
#   4. Okay than just use its extension.
#   5. No extension?, maybe can you execute it? -> executable
# please speficy exact types like image/jpeg
# before speficing the general one
file_types:
    - { type: '^(dir|link)$',      icon: '',  action:    'cd',                  preview_stdout: 'ls %f',                     color: 'yellow' } 
    - { type: '^sh$',              icon: '',  exec_term: 'nvim %f',             preview:        'raw',                       color: 'green' } 
    - { type: '^executable',       icon: '',  exec_term: '%f',                  preview_stdout: 'file %f',                   color: 'green' } 
    - { type: '^application/pdf$', icon: '',  exec:      'zathura %f',          preview_stdout: 'pdftotext %f -',            color: 'blue' } 
    - { type: '^audio/mpeg$',      icon: '',  exec:      'ffplay %f',           preview_stderr: 'ffmpeg -i %f -hide_banner', color: 'red' } 
    - { type: '^go$',              icon: '',  exec_term: 'nvim %f',             preview:        'raw',                       color: 'blue' } 
    - { type: '^application/zip$', icon: '',  exec_term: 'unzip %f',            preview_stdout: 'unzip -l %f',               color: 'green' } 
    - { type: 'tar.gz$',           icon: '',  exec_term: 'tar -xvf %f',         preview_stdout: 'tar -tvf %f',               color: 'green' } 
    - { type: '^rmd$',             icon: '',  exec_term: 'nvim %f',             preview:        'raw',                       color: 'blue' } 
    - { type: '^text/',            icon: '',  exec_term: 'nvim %f',             preview:        'raw',                       color: 'blue' } 
    - { type: '^image/svg',        icon: '',  exec:      'imv %f',              preview_stdout: 'identify %f',               color: 'red' } 
    - { type: '^image/',           icon: '',  exec:      'imv %f',              preview:        'ubz',                       color: 'green' } 
    - { type: '^video/',           icon: '',  exec:      'ffplay %f',           preview_stderr: 'ffmpeg -i %f -hide_banner', color: 'red' } 
    - { type: '^audio/',           icon: '',  exec_term: 'aplay -D pulse %f',   preview_stderr: 'ffmpeg -i %f -hide_banner', color: 'red' } 
    - { type: '^.*$',              icon: '',  exec:      'xdg-open %f',         preview_stdout: 'file %f',                   color: 'default' } 

# Key bindings
bindings:
    - { key: 'q', action: quit }
    - { key: 'j', action: move_down }
    - { key: '<down>', action: move_down }
    - { key: '<st-down>', action: move_down }
    - { key: 'k', action: move_up }
    - { key: '<up>', action: move_up }
    - { key: '<st-up>', action: move_up }
    - { key: 'l', action: open }
    - { key: '<ret>', action: open }
    - { key: '<right>', action: open }
    - { key: 'h', action: back }
    - { key: '<left>', action: back }
    - { key: '<left>', action: back }
    - { key: 'S', action: exec_term }
    - { key: '<smaller>', action: widen_preview }
    - { key: '<greather>', action: shorten_preview }
    - { key: 'zz',  action: recenter }
    - { key: 'gg',  action: move_top }
    - { key: 'gh',  action: go_home }
    - { key: 'G', action: move_bottom }
    - { key: 'f', action: quick_run }
    - { key: '<slash>', action: filter }
    - { key: '<cn>', action: hide_preview }

```
